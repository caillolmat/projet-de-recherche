import numpy as np
from os import listdir
from os.path import isdir, join
from pickle import load
from random import sample, shuffle
import random
from sklearn.metrics import confusion_matrix
# from tensorflow import keras
import math
from collections import Counter
import tensorflow as tf
from tensorflow.keras.utils import to_categorical

def count4features(len_f):
    a = np.ones(len_f) #11385
    a[0:5] = [35, 100, 9587, 758, 54]
    return a


def count5features(len_f):
    a = np.ones(len_f) # 5008
    a[0:4] = [54, 758, 9587, 35]
    return a

def count6features(len_f):
    a = np.ones(len_f) #1196
    a[0:6] = [ 100, 54 ,35 ,758, 9587, 121]
    return a
processing_data_switcher = {
    'Drebin-Dataset/observations-4count' : count5features,
    'Drebin-Dataset/observations-5count' : count4features,
    'Drebin-Dataset/observations-6count' : count6features
}

class NNData:
    """
    Provides an interface to load training and test datasets for `partial_fit` directly from the disk.
    """

    training_paths = None
    input_dim = None
    array_normalization = None
    validation_paths = None
    test_path = None
    class_weights = None
    output_dim = 11
    multiple_file = True
    counter_class = None

    def __init__(self, directory='gen/observations', train_split=0.7, tv_split=0.5, nbr_classes = None):
        self.directory = directory
        self.paths = [join(directory, f) for f in listdir(directory)]
        if nbr_classes is not None:
            self.output_dim = nbr_classes
        if len(self.paths) == 1:
            self.multiple_file = False
            with open(self.paths[0], 'rb') as file:
                x = load(file)
            shuffle(x)
            n = len(x)
            train_n = math.ceil(n * train_split)
            test_n = math.ceil((n - train_n) * tv_split)
            valid_n = n - (train_n + test_n)
            test_n = test_n
            valid_n = valid_n
            self.training_paths = x[0:train_n]
            del x[0:train_n]
            self.test_path = x[0:test_n]
            del x[0:test_n]
            self.validation_paths =x[0:valid_n]
            del x[0:valid_n]
        else:
            self.multiple_file = True
            shuffle(self.paths)
            train_n = math.ceil(len(self.paths) * train_split)
            test_n = math.ceil((len(self.paths) - train_n) * tv_split)
            valid_n = len(self.paths) - (train_n + test_n)
            test_n = train_n + test_n
            valid_n = train_n + test_n + valid_n
            self.training_paths = self.paths[0:train_n]
            self.test_path = self.paths[train_n:test_n]
            self.validation_paths = self.paths[test_n:valid_n]


    def get_input_dim(self):
        """
        Returns the input dimension for a neural network (ie the dimensionality of the input vectors).
        :return: The input dimension for a neural network (ie the dimensionality of the input vectors).
        """
        if self.input_dim is None:
            if self.multiple_file:
                with open(self.paths[0], 'rb') as file:
                    x = load(file)
                    x = np.array(x)
                    self.input_dim = len(x[0]) - 1
                    return len(x[0]) - 1
            else:
                self.input_dim = len(self.test_path[0])- 1
                return self.input_dim
        else:
            return self.input_dim


    def training_generator(self, amount=10, batch_size=250):
        """
        A generator that yields `amount` training set batches.
        :param amount: The amount of batches to yield.
        :return: Two numpy arrays: One containing the batch training observations and another containing the batch
        labels.
        """
        paths = self.training_paths
        if self.multiple_file:
            return self.multiple_file_gen(paths, batch_size)
        else:
            return self.single_file_gen(paths, batch_size)

    def validation_generator(self, batch_size):
        paths = self.validation_paths
        if self.multiple_file:
            return self.multiple_file_gen(paths, batch_size)
        else:
            return self.single_file_gen(paths, batch_size)

    def get_test_data(self):
        """
        Gets a test dataset that is independent from the generated batches in `training_generator`.
        :param amount: The amount of batches to load. If `None`, takes all available batches that were not used in
        `training_generator`.
        :return: Two numpy arrays: One containing the test observations and another containing the test labels.
        """
        # paths = [join(self.directory, f)
        #          for f in listdir(self.directory) if join(self.directory, f) not in self.training_paths]
        paths = self.test_path
        x = []
        if self.multiple_file:
            for path in paths:
                with open(path, 'rb') as file:
                    x += load(file)
        else:
            x = self.test_path
        x = np.array(x)
        x_te = x[:, :-1]/self.array_normalization
        y_te = x[:, -1]
        y_te = np.eye(self.output_dim)[np.minimum(y_te, self.output_dim - 1)]
        return x_te, y_te


    def single_file_gen(self, paths, batch_size):
        batchcount = 0
        X = []
        Y = []
        x_tr = np.array([])
        y_tr = np.array([])
        while True:
            for i in paths:

                X.append(i[0:-1])
                Y.append(i[-1])
                batchcount += 1
                if batchcount >= batch_size:
                    x_tr = np.array(X) / self.array_normalization
                    y_tr = np.eye(self.output_dim)[np.minimum(np.array(Y), self.output_dim - 1)]
                    yield x_tr, y_tr
                    X = []
                    Y = []
                    batchcount = 0
            shuffle(paths)


    def multiple_file_gen(self,paths, batch_size):
        batchcount = 0
        count = 0
        X = []
        Y = []
        lperm = np.random.permutation(len(paths))

        while True:
            i = lperm[0]
            with open(paths[i], 'rb') as file:
                x = load(file)
                li = len(x)
                for j in range(li):
                    # x = np.array(x)
                    X.append(x[j][0:-1])
                    Y.append(x[j][-1])
                    batchcount += 1
                    # x_tr = x[:, :-1]
                    # y_tr = x[:, -1]
                    if batchcount >= batch_size or j >= li - 1:
                        x_tr = np.array(X) / self.array_normalization
                        y_tr = np.eye(self.output_dim)[np.minimum(np.array(Y), self.output_dim - 1)]
                        # print(x_tr.shap)
                        yield x_tr, y_tr
                        X = []
                        Y = []
                        batchcount = 0
                count += 1
                if count == len(paths):
                    count = 0

                    lperm = np.random.permutation(len(paths))

    def data_preprocessing(self, calcul=True, normalize=True, inbalanced=True):
        if normalize:
            if calcul:

                if self.array_normalization is None:
                    self.array_normalization = np.zeros(self.get_input_dim(), dtype=int)
                    for path in self.paths:
                        print('Normalisation step ', path)
                        with open(path, 'rb') as file:
                            x = np.array(load(file))[:, :-1]
                            x = x.max(axis=0)
                            self.array_normalization = np.maximum(x, self.array_normalization)
            else:
                self.array_normalization = processing_data_switcher.get(self.directory)(self.get_input_dim())

        else:
            self.array_normalization = np.ones(self.get_input_dim())
        if inbalanced:
            counter = {}

            for path in self.paths:
                with open(path, 'rb') as file:
                    x = np.array(load(file))[:,-1]
                    c = Counter(x)
                    counter.update(dict((key, c.get(key) + counter.get(key, 0)) for key in c.keys()))
                    print('file', path, 'end with', self.paths[-1])

            self.counter_class = counter
            counter = {}
            for i in range(self.output_dim):
                counter.update({i: self.counter_class.get(i)})
            for i in range(self.output_dim, len(self.counter_class)):
                counter.update({self.output_dim - 1: counter.get(self.output_dim - 1, 0) + self.counter_class.get(i)})
            m = max(list(counter.values()))
            self.class_weights = dict((key, m/counter.get(key)) for key in counter.keys())
        else:
            self.counter_class = {5: 339, 0: 123453, 4: 613, 10: 1630, 1: 925, 3: 625, 2: 667, 8: 147, 9: 132, 6: 330, 7: 152}
            counter = {}
            for i in range(self.output_dim):
                counter.update({i: self.counter_class.get(i)})
            for i in range(self.output_dim,len(self.counter_class)):
                counter.update({self.output_dim-1: counter.get(self.output_dim-1,0) + self.counter_class.get(i)})

            m = max(list(counter.values()))
            self.class_weights = dict((key, m/counter.get(key)) for key in counter.keys())
            # self.class_weights = {2: 185.08695652173913, 0: 1.0, 1: 133.4627027027027, 10: 75.73803680981595, 6: 374.1, 7: 812.1907894736842, 3: 197.5248, 4: 201.3915171288744, 8: 839.8163265306123, 5: 364.16814159292034, 9: 935.25}

    # def resampling_batch_during_runtime(self, array):



def load_observations(amount=1, directory='gen/observations', mode='firstk'):
    """
    Loads the observations from the specified directory.
    :param amount: The amount of files which should be loaded. Be careful with high numbers, as the observations quickly
                    exceed the memory limit. Needs to be a positive integer.
    :param directory: The directory to load observations from.
    :param mode: Specifies how the `amount` batches should be drawn from all batches. Possible values:
    * firstk: Takes the first `amount` batches
    * sample: Samples `amount` batches uniformly from all batches
    * lastk: Takes the last `amount` batches
    :return: A two-dimensional numpy array containing the observations in rows and their features in columns.
    """
    if amount < 1:
        raise ValueError('Amount needs to be a positive integer')
    if not isdir(directory):
        raise ValueError('Directory must exist')
    paths = [join(directory, f) for f in listdir(directory)]
    if mode == 'firstk' or mode == 'lastk':
        paths = sorted(paths)
        if mode == 'lastk':
            paths = reversed(paths)
        x = []
        for i in range(amount):
            with open(paths[i], 'rb') as file:
                x += load(file)
    elif mode == 'sample':
        batches = sample(paths, amount)
        x = []
        for batch in batches:
            with open(batch, 'rb') as file:
                x += load(file)
    return np.array(x)


def split_data(x, train_pct):
    """
    Splits observations in a training and test dataset.
    :param x: The observations as numpy array that contain class labels in the last row.
    :param train_pct: The percentage of observations to use for training. 1 - `training_pct` is used for the test set.
    :return: Four numpy arrays:
            1. Training observations without class labels
            2. Test observations without class labels
            3. Trainings class labels
            4. Test class labels
    """
    y_tr, y_te = None, None
    while y_te is None or 1 not in y_te:  # Ensure that test dataset does not contain solely benignware
        training_size = int(len(x) * train_pct)
        indices = np.random.permutation(len(x))
        training_idx, test_idx = indices[:training_size], indices[training_size:]
        x_tr, x_te = x[training_idx, :], x[test_idx, :]
        y_tr, y_te = x_tr[:, -1], x_te[:, -1]
        x_tr, x_te = x_tr[:, :-1], x_te[:, :-1]  # Last column is label column
    return x_tr, x_te, y_tr, y_te


def measure_performance(predictions, ground_truth):
    """
    Measures the performance of a Support Vector Machine model.
    :param s: The Support Vector Machine that was already trained.
    :param test_observations: The observations of the test dataset without class labels.
    :param ground_truth: The class labels of the test dataset.
    :return: The True Positive Rate (True Positives / Real Positives) and
            False Positive Rate (False Positives / Real Negatives)
    """
    cm = confusion_matrix(ground_truth, predictions)
    tn = cm[0][0]
    fn = cm[0][1]
    fp = cm[1][0]
    tp = cm[1][1]
    if (tp + fn) == 0:
        tpr = 0
    else:
        tpr = tp / (tp + fn)
    if fp + tn == 0:
        fpr = 0
    else:
        fpr = fp / (fp + tn)
    nb_class = len(cm)
    print('C', sep='\t', end='\t')
    for i in range(nb_class):
        print(i, sep='\t', end='\t')
    print("")
    for i in range(nb_class):
        print(i, sep='\t', end='\t')
        for val in cm[i]:
            print(val, sep='\t', end='\t')
        print("")
    print('True positives', tp, 'False positives', fp, 'True negatives', tn, 'False negatives', fn)
    return tpr, fpr
