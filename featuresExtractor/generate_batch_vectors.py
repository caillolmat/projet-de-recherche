#!/usr/bin/env python3
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from csv import reader
from math import ceil
from pickle import dump, load
from os import listdir, mkdir
from os.path import basename, isdir, join, splitext
import pandas

def countf(feature, lines, observation):
    feature_head = feature.split('::')[0]
    nbr_feature = len([x for x in lines if feature_head == x[0:len(feature_head)]])
    observation.append(nbr_feature)


def allf(feature, lines, observation):
    if feature in lines:
        observation.append(1)
    else:
        observation.append(0)


def nonef(feature, lines, observation):
    print('none feature ??')
    pass


def unknown(feature, lines, observation):
    print('Unknown feature', feature)


feature_set = {
    'feature': allf,  # Hardware Components 72
    'permission': countf,  # Requested Permission 3812
    'activity': countf,  # App Components p1 185729
    'service_receiver': countf,  # App Component p2 33222
    'provider': countf,  # App Component p3 : 4513
    'intent': countf,  # Filtered Intent 6379
    'call': allf,  # Restricted Api Calls 733
    'real_permission': allf,  # Used Permission 70
    'api_call': allf,  # = Suspicious Api Calls 315
    'url': countf,  # Network Address 310488

}

dataset_path = 'Drebin-Dataset'
features_path = 'Drebin-Dataset/generated/features-u-pr-sr-ac-i-per_count.p'
labels_path = 'Drebin-Dataset/sha256_family.csv'
in_directory = 'Drebin-Dataset/feature_vectors'
observations_per_file = 132000
out_directory = 'Drebin-Dataset/observations-6count'
startwith = 0

def generate_vectors(features_path=features_path, labels_path=labels_path, in_directory=in_directory,
                     observations_per_file=observations_per_file, out_directory=out_directory, topclasses=0):
    if not isdir(out_directory):
        mkdir(out_directory)
    with open(features_path, 'rb') as file:
        features = load(file)
    if labels_path is not None:
        p_observations = pandas.read_csv(labels_path,index_col='sha256')
        if topclasses == 0:
            p_observations = p_observations.assign(nbr_class=1)
        else:
            top = list(p_observations.family.value_counts().head(topclasses).keys())
            p_observations = p_observations.assign(nbr_class=lambda x: x.family.apply(lambda y: top.index(y) + 1 if y in top else len(top) + 1))
    paths = [join(in_directory, f) for f in listdir(in_directory)]
    fill_nr = len(str(len(paths) // observations_per_file))
    last_nr = 0
    if isdir(out_directory) and len(listdir(out_directory)) > 0:
        last_nr = sorted(listdir(out_directory))[-1]
        last_nr = int(last_nr.split('.')[0])
    observations = []
    for i in range(startwith, len(paths)):
        path = paths[i]
        print('Observation ' + str(i) + ' of ' + str(len(paths)), path)
        with open(path, 'r') as file:
            observation = []
            lines = file.read().splitlines()
            for feature in features:
                switcher = feature_set.get(feature.split('::')[0], 'unknown')
                switcher(feature, lines, observation)



            if labels_path is not None:
                sha = splitext(basename(path))[0]
                if sha in p_observations.index:
                    observation.append(p_observations.loc[sha].nbr_class)
                else:
                    observation.append(0)
            observations.append(observation)
        if (i + 1) % observations_per_file == 0:
            filename = str((i + 1 + last_nr) // observations_per_file).zfill(fill_nr) + '.p'
            with open(join(out_directory, filename), 'wb') as file:
                dump(observations, file)
            observations = []

    if len(observations) > 0:  # Dump the rest observations
        filename = str(int(ceil(i + last_nr / observations_per_file))) + '.p'
        with open(join(out_directory, filename), 'wb') as file:
            dump(observations, file)


if __name__ == "__main__":
    parser = ArgumentParser(description='Generates python vectors from string observations',
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('-n', '--number', type=int, default=observations_per_file, help='Number of observations per '
                                                                                        'outputted file. The higher '
                                                                                        'the number, the higher the '
                                                                                        'memory requirements.')
    parser.add_argument('-i', '--input', type=str, default=in_directory, help='The directory in which the input '
                                                                              'observations in their raw string format '
                                                                              'are located. Must be the same directory '
                                                                              'that was also used as input directory for '
                                                                              '`acc_features.py`.')
    parser.add_argument('-l', '--labels', type=str, default=labels_path, help='The path to a CSV file that assigns the '
                                                                              'positive class label to certain '
                                                                              'observations contained in the input '
                                                                              'directory. It contains two columns: '
                                                                              'An observation id column and a malware '
                                                                              'family column. Pass `None` to skip'
                                                                              'labelling')
    parser.add_argument('-f', '--features', type=str, default=features_path,
                        help='The path to the file which contains all '
                             'possible features in a binary format. '
                             'This should be the output file of '
                             '`acc_features.py`.')
    parser.add_argument('-o', '--output', type=str, default=out_directory, help='The directory which the output files '
                                                                                'should be written to.')
    parser.add_argument('-t', '--topclass', type=int, default=9, help='The classification you want (binary=0 / topclass = 1..179 ).'
                                                                      'If you want top 10 classes : 0-9 will be the top classes, 10 will be others and 11 will be benin')
    args = parser.parse_args()
    features_path = args.features
    labels_path = args.labels
    in_directory = args.input
    observations_per_file = args.number
    out_directory = args.output
    top_classes = args.topclass
    generate_vectors(features_path, labels_path, in_directory, observations_per_file, out_directory, top_classes)
