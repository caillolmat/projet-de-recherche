#!/usr/bin/env python3
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from pickle import dump
from os import listdir, mkdir
from os.path import isdir, join

in_directory = 'Drebin-Dataset/feature_vectors'
out_directory = 'Drebin-Dataset/generated'


def countf(features, line):
    features.get('count').add(line[0] + '::' + 'number')

def allf(features, line):
    features.get('all').add(line[0] + '::' + line[1])
    # print('Accumulated feature', len(features.get('all')))


def nonef(features, line):
    pass


def unknown(features, line):
    if len(line) == 2:
        print('Unknown feature : ', line[0] + '::' + line[1])
        if len((line[0] + line[1]).strip()):
            exit(1)


feature_set = {
    'feature': allf,  # Hardware Components 72
    'permission': countf,  # Requested Permission 3812
    'activity': countf,  # App Components p1 185729
    'service_receiver': countf,  # App Component p2 33222
    'provider': countf,  # App Component p3# : 4513
    'intent': countf,  # Filtered Intent 6379
    'call': allf,  # Restricted Api Calls 733
    'real_permission': allf,  # Used Permission 70
    'api_call': allf,  # = Suspicious Api Calls 315
    'url': countf,  # Network Address 310488

}


def accumulate_features(in_dir=in_directory, out_dir=out_directory):
    if not isdir(out_dir):
        mkdir(out_dir)
    paths = [join(in_dir, f) for f in listdir(in_dir)]
    features = {'all': set(), 'count': set()}
    for idx, path in enumerate(paths):
        with open(path, 'r') as file:
            lines = file.read().splitlines()
            lines = [sl.split('::') for sl in lines]
            for line in lines:
                switcher = feature_set.get(line[0], unknown)
                switcher(features, line)
        if idx % 1000 == 0:
            print(idx, "out of", len(paths), "files treated")
    print(len(features.get('all')))
    with open(join(out_dir, 'features-u-pr-sr-ac-i-per_count.p'), 'wb') as file:
        flatfeature = []
        flatfeature.extend(list(features.get('count')))
        flatfeature.extend(list(features.get('all')))
        dump(flatfeature, file)


if __name__ == '__main__':
    parser = ArgumentParser(
        description='Accumulates all possible features of the observations in sparse string format ',
        formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('-i', '--input', type=str, default=in_directory, help='The directory in which the input '
                                                                              'observations in their raw string format '
                                                                              'are located.')
    parser.add_argument('-o', '--output', type=str, default=out_directory, help='The directory which the output file '
                                                                                'should be written to.')
    args = parser.parse_args()
    in_directory = args.input
    out_directory = args.output
    accumulate_features(in_directory, out_directory)
